import 'dart:io';
import 'dart:math';

String inputS(String S) {
  return S = stdin.readLineSync()!;
}

List cutString(List sent, String S) {
  return sent = S.split(" ");
}

void showResult(List sent) {
  return print(sent);
}

void main(List<String> arguments) {
  print('input your word:');
  String S = " ";
  List sent = [];
  S = inputS(S);
  sent = cutString(sent, S);
  showResult(sent);
  List sent2 = postfix(sent);
  print(sent2);
  print(Evaluate(sent2));
}

List postfix(List sent) {
  List oper = [];
  List pofix = [];
  int token = 0;
  int operlast = 0;

  for (var i = 0; i < sent.length; i++) {
    switch (sent[i]) {
      case "0":
        pofix.add(sent[i]);
        break;
      case "1":
        pofix.add(sent[i]);
        break;
      case "2":
        pofix.add(sent[i]);
        break;
      case "3":
        pofix.add(sent[i]);
        break;
      case "4":
        pofix.add(sent[i]);
        break;
      case "5":
        pofix.add(sent[i]);
        break;
      case "6":
        pofix.add(sent[i]);
        break;
      case "7":
        pofix.add(sent[i]);
        break;
      case "8":
        pofix.add(sent[i]);
        break;
      case "9":
        pofix.add(sent[i]);
        break;
      default:
    }
    if (sent[i] == "+" ||
        sent[i] == "-" ||
        sent[i] == "*" ||
        sent[i] == "/" ||
        sent[i] == "^") {
      switch (sent[i]) {
        case "+":
          token = 1;
          break;
        case "-":
          token = 1;
          break;
        case "*":
          token = 2;
          break;
        case "/":
          token = 2;
          break;
        case "^":
          token = 3;
          break;

        default:
      }
      if (oper.isNotEmpty) {
        switch (oper.last) {
          case "+":
            operlast = 1;
            break;
          case "-":
            operlast = 1;
            break;
          case "*":
            operlast = 2;
            break;
          case "/":
            operlast = 2;
            break;
          case "^":
            operlast = 3;
            break;

          default:
        }
      }
      while (oper.isNotEmpty && oper.last != "(" && token <= operlast) {
        pofix.add(oper.removeLast());
      }
      oper.add(sent[i]);
    }
    if (sent[i] == "(") {
      oper.add(sent[i]);
    }
    if (sent[i] == ")") {
      while (oper.last != "(") {
        pofix.add(oper.removeLast());
      }
      oper.remove("(");
    }
  }
  while (oper.isNotEmpty) {
    pofix.add(oper.removeLast());
  }
  return pofix;
}

Evaluate(List sent) {
  List<double> values = [];

  for (var i = 0; i < sent.length; i++) {
    if (sent[i] == "0" ||
        sent[i] == "1" ||
        sent[i] == "2" ||
        sent[i] == "3" ||
        sent[i] == "4" ||
        sent[i] == "5" ||
        sent[i] == "6" ||
        sent[i] == "7" ||
        sent[i] == "8" ||
        sent[i] == "9") {
      double num = double.parse(sent[i]);
      values.add(num);
    } else {
      double sum = 0;
      double right = values.last;
      values.removeLast();
      double left = values.last;
      values.removeLast();

      if (sent[i] == "+") {
        sum = left + right;
      }
      if (sent[i] == "-") {
        sum = left - right;
      }
      if (sent[i] == "*") {
        sum = left * right;
      }
      if (sent[i] == "/") {
        sum = left / right;
      }
      if (sent[i] == "^") {
        sum = 0.0 + pow(left, right);
      }
      values.add(sum);
    }
  }
  return values[0];
}
